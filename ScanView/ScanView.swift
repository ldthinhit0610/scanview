//
//  ScanView.swift
//  ScanView
//
//  Created by Luu Duc Thinh on 23/11/2020.
//

import Foundation
import UIKit

class ScanView: UIView {
    @IBInspectable weak var linePathColor: UIColor? = UIColor.green
    @IBInspectable var lineWidth: CGFloat = 4
    @IBInspectable var lineLength: CGFloat = 44

    override func draw(_ rect: CGRect) {
        createPath()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func createPath() {
        let path = UIBezierPath()
        let height: CGFloat = self.frame.size.height
        let width: CGFloat = self.frame.size.width
        path.move(to: .zero)
        path.move(to: CGPoint(x: 0, y: lineLength))
        path.addLine(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: lineLength, y: 0))
        path.move(to: CGPoint(x: width - lineLength, y: 0))
        path.addLine(to: CGPoint(x: width, y: 0))
        path.addLine(to: CGPoint(x: width, y: lineLength))
        path.move(to: CGPoint(x: width, y: height - lineLength))
        path.addLine(to: CGPoint(x: width, y: height))
        path.addLine(to: CGPoint(x: width - lineLength, y: height))
        path.move(to: CGPoint(x: 0 + lineLength, y: height))
        path.addLine(to: CGPoint(x: 0, y: height))
        path.addLine(to: CGPoint(x: 0, y: height - lineLength))
        path.move(to: .zero)
        path.lineWidth = lineWidth
        UIColor.clear.setFill()
        path.fill()
        // Specify a border (stroke) color.
        linePathColor?.setStroke()
        path.stroke()
        path.close()
    }
}
